--drop FUNCTION [dbo].[f_get_xsd_afas_update_organisation]

CREATE FUNCTION [dbo].[f_get_xsd_afas_update_organisation](@Relatie_NR varchar(50), @Action varchar(50), @Omgeving varchar(50)) returns xml
AS
BEGIN

	DECLARE @DbId varchar(50);
	IF(@Omgeving = 'Trade')
	BEGIN
		SET @DbId = ISNULL(NULLIF((SELECT AFAS_ID_TRADE FROM AFAS.AFAS_DATA WHERE RBS_RELATIE_NR = @Relatie_NR), ''),null);
	END
	ELSE IF(@Omgeving = 'Service')
	BEGIN
		SET @DbId = ISNULL(NULLIF((SELECT AFAS_ID_SERVICE FROM AFAS.AFAS_DATA WHERE RBS_RELATIE_NR = @Relatie_NR), ''),null);
	END
	ELSE IF(@Omgeving = 'Uienpool')
	BEGIN
		SET @DbId = ISNULL(NULLIF((SELECT AFAS_ID_UIENPOOL FROM AFAS.AFAS_DATA WHERE RBS_RELATIE_NR = @Relatie_NR), ''),null);
	END
	ELSE IF(@Omgeving = '')
	BEGIN
		SET @DbId = (SELECT Autonummer + 10000 FROM dbo.relatie WHERE RELATIE_NR = @Relatie_NR AND Relatietype = 16);
	END

	IF(@DbId IS NULL)
	BEGIN
		SET @DbId = (SELECT 10000 FROM dbo.relatie WHERE RELATIE_NR = @Relatie_NR AND Relatietype = 16);
	END


	DECLARE @xsd XML(AFAS_KnOrganisation_XSD);
	--DECLARE @xsd XML;

	IF(@DbId IS NOT NULL)
	BEGIN

	SELECT @xsd = 
		(SELECT (
					SELECT (
					
					SELECT @Action as "@Action",
						  0 AS PadAdr
						, 0 AS AutoNum
						, 0 AS MatchOga
						, 0 AS BcId
						, (Autonummer + 10000) AS BcCo
						, Bedrijf_Naam AS Nm
						, ISNULL(NULLIF(KVKNummer, ''),null) AS CcNr
						, ISNULL(NULLIF(Bedrijf_Telefoon, ''),null) AS TeNr
						, ISNULL(NULLIF(Bedrijf_Telefax, ''),null) AS FaNr
						, ISNULL(NULLIF(Bedrijf_Mobiel, ''),null) AS MbNr
						--, ISNULL(NULLIF(Bedrijf_Email, ''),null) AS EmAd
						, ISNULL(NULLIF(Bedrijf_Homepage,''),null) AS HoPa
						, 0 AS Corr
						--, ISNULL(NULLIF(Overig,''),null) AS Re
					FROM dbo.RELATIE
					WHERE relatie.Relatie_NR = @RELATIE_NR AND
					      relatie.Relatietype = 16
					FOR XML PATH('Fields'), type
					),
	
					( SELECT
						
						(SELECT
							(SELECT
								(SELECT @Action as "@Action",
									(select top 1 land.Landcode
									 from dbo.land, dbo.relatie
									 where relatie.Factuur_Land = land.Land_ID)
									 AS CoId
									 , 1 AS IbCk
									 , ISNULL(NULLIF(Bankrekening, ''), null) AS Iban
									 , 0 AS AcGa
									 , 0 AS AcCk
									 , iif(len(BIC) = 0, null, BIC) AS Bic
								FROM [dbo].[RELATIE]
								WHERE relatie.Relatie_NR = @RELATIE_NR AND
									  relatie.Relatietype = 16
								FOR XML PATH('Fields'), type)
							FOR XML PATH('Element'), type)
						FOR XML PATH('KnBankAccount'), type),
						
						(SELECT
							( SELECT
								( SELECT @Action as "@Action",
									(select top 1 land.Landcode
									 from dbo.land, dbo.relatie
									 where relatie.Vestiging_Land = land.Land_ID)
									 AS CoId
									, 0 AS PbAd
									, IIF( LEN( (RTRIM((Vestiging_Adres_1 + ' ' + ISNULL(Vestiging_Adres_2, '')))) ) = 0, null, (RTRIM((Vestiging_Adres_1 + ' ' + ISNULL(Vestiging_Adres_2, ''))))) AS Ad
									, ISNULL(NULLIF(afas.f_get_adrespart(Vestiging_Adres_NR, 'nummer'),''),null) AS HmNr
									, ISNULL(NULLIF(afas.f_get_adrespart(Vestiging_Adres_NR, 'toevoeging'),''),null) AS HmAd
									, iif(len(Vestiging_Postcode)=0, null, Vestiging_Postcode) AS ZpCd
									, iif(len(Vestiging_Plaats) = 0, null, Vestiging_Plaats)  AS Rs
									, 0 AS ResZip
								FROM [dbo].[RELATIE]
								WHERE relatie.Relatie_NR = @RELATIE_NR AND
									  relatie.Relatietype = 16
								FOR XML PATH('Fields'), type)
							FOR XML PATH('Element'), type)
						FOR XML PATH('KnBasicAddressAdr'), type),
	
						(SELECT
							( SELECT
								( SELECT @Action as "@Action",
									(select top 1 land.Landcode
									 from dbo.land, dbo.relatie
									 where relatie.Postadres_Land = land.Land_ID)
									 AS CoId
									, 0 AS PbAd
									, IIF( LEN( (RTRIM((Postadres_1 + ' ' + ISNULL(Postadres_2,'')))) ) = 0, null, (RTRIM((Postadres_1 + ' ' + ISNULL(Postadres_2,''))))) AS Ad
									, ISNULL(NULLIF(afas.f_get_adrespart(Postadres_NR, 'nummer'),''),null) AS HmNr
									, ISNULL(NULLIF(afas.f_get_adrespart(Postadres_NR, 'toevoeging'),''),null) AS HmAd
									, iif(len(Postadres_Postcode) = 0, null, Postadres_Postcode) AS ZpCd
									, iif(len(Postadres_Plaats) = 0, null, Postadres_Plaats) AS Rs
									, 0 AS ResZip
								FROM [dbo].[RELATIE]
								WHERE relatie.Relatie_NR = @RELATIE_NR AND
									  relatie.Relatietype = 16
								FOR XML PATH('Fields'), type)
							FOR XML PATH('Element'), type)
						FOR XML PATH('KnBasicAddressPad'), type)
					FOR XML PATH('Objects'), type
				) FOR XML PATH('Element'), type			
	) FOR XML RAW('KnOrganisation'));	

	END

	return @xsd;

END