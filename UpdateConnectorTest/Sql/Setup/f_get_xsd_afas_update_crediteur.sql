USE [RBS3                                                                                                                            ]
GO

/****** Object:  UserDefinedFunction [dbo].[f_get_xsd_afas_update_crediteur]    Script Date: 4/24/2014 1:26:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE FUNCTION [dbo].[f_get_xsd_afas_update_crediteur](@Relatie_NR varchar(50), @Action varchar(50), @Omgeving varchar(50)) returns xml
AS
BEGIN

DECLARE @CrId varchar(50);
IF(@Omgeving = 'Trade')
BEGIN
SET @CrId = ISNULL(NULLIF((SELECT AFAS_ID_TRADE FROM dbo.AFAS_DATA WHERE RBS_RELATIE_NR = @Relatie_NR), ''),null);
END
ELSE IF(@Omgeving = 'Service')
BEGIN
SET @CrId = ISNULL(NULLIF((SELECT AFAS_ID_SERVICE FROM dbo.AFAS_DATA WHERE RBS_RELATIE_NR = @Relatie_NR), ''),null);
END
ELSE IF(@Omgeving = 'Uienpool')
BEGIN
SET @CrId = ISNULL(NULLIF((SELECT AFAS_ID_UIENPOOL FROM dbo.AFAS_DATA WHERE RBS_RELATIE_NR = @Relatie_NR), ''),null);
END
ELSE IF(@Omgeving = '')
BEGIN
SET @CrId = (SELECT Autonummer + 10000 FROM dbo.relatie WHERE RELATIE_NR = @Relatie_NR AND Relatietype = 16);
END

IF(@CrId IS NULL)
BEGIN
SET @CrId = (SELECT Autonummer + 10000 FROM dbo.relatie WHERE RELATIE_NR = @Relatie_NR AND Relatietype = 16);
END

DECLARE @xsd XML(AFAS_KnPurchaseRelationOrg_XSD)


IF(@CrId IS NOT NULL)
BEGIN
SELECT @xsd = ( 
		SELECT @CrId AS "CrId",
			(SELECT @Action AS '@Action',
				 'true' AS IsCr
				, ISNULL(NULLIF(BTWNummer, ''),null) AS VaId
				, 14 AS PaCd											/**betalingstermijn**/
				, 1600 AS ColA											/**voorkeursrekening: 1300 debiteuren, 1600 crediteuren**/
			 FROM dbo.RELATIE
			 WHERE relatie.Relatie_NR = @RELATIE_NR AND
				   relatie.Relatietype = 16
			 FOR XML PATH('Fields'), type
			),
			
			(SELECT (
				SELECT (
					SELECT (
					
					SELECT @Action as "@Action",
						  0 AS PadAdr
						, 0 AS AutoNum
						, 0 AS MatchOga
						, 0 AS BcId
						, @CrId AS BcCo
						, Bedrijf_Naam AS Nm
						, ISNULL(NULLIF(KVKNummer, ''),null) AS CcNr
						, ISNULL(NULLIF(Bedrijf_Telefoon, ''),null) AS TeNr
						, ISNULL(NULLIF(Bedrijf_Telefax, ''),null) AS FaNr
						, ISNULL(NULLIF(Bedrijf_Mobiel, ''),null) AS MbNr
						--, ISNULL(NULLIF(Bedrijf_Email, ''),null) AS EmAd
						, ISNULL(NULLIF(Bedrijf_Homepage,''),null) AS HoPa
						, 0 AS Corr
						--, ISNULL(NULLIF(Overig,''),null) AS Re
					FROM dbo.RELATIE
					WHERE relatie.Relatie_NR = @RELATIE_NR AND
					      relatie.Relatietype = 16
					FOR XML PATH('Fields'), type
					),
	
					( SELECT
						
						(SELECT
							(SELECT
								(SELECT @Action as "@Action",
									(select top 1 land.Landcode
									 from dbo.land, dbo.relatie
									 where relatie.Factuur_Land = land.Land_ID)
									 AS CoId
									 , 1 AS IbCk
									 , ISNULL(NULLIF(Bankrekening, ''), null) AS Iban
									 , 0 AS AcGa
									 , 0 AS AcCk
									 , BIC AS Bic
								FROM [dbo].[RELATIE]
								WHERE relatie.Relatie_NR = @RELATIE_NR AND
									  relatie.Relatietype = 16
								FOR XML PATH('Fields'), type)
							FOR XML PATH('Element'), type)
						FOR XML PATH('KnBankAccount'), type),
						
						(SELECT
							( SELECT
								( SELECT @Action as "@Action",
									(select top 1 land.Landcode
									 from dbo.land, dbo.relatie
									 where relatie.Vestiging_Land = land.Land_ID)
									 AS CoId
									, 0 AS PbAd
									, (RTRIM((Vestiging_Adres_1 + ' ' + ISNULL(Vestiging_Adres_2, '')))) AS Ad
									, ISNULL(NULLIF(dbo.f_get_adrespart(Vestiging_Adres_NR, 'nummer'),''),null) AS HmNr
									, ISNULL(NULLIF(dbo.f_get_adrespart(Vestiging_Adres_NR, 'toevoeging'),''),null) AS HmAd
									, Vestiging_Postcode AS ZpCd
									, Vestiging_Plaats AS Rs
									, 0 AS ResZip
								FROM [dbo].[RELATIE]
								WHERE relatie.Relatie_NR = @RELATIE_NR AND
									  relatie.Relatietype = 16
								FOR XML PATH('Fields'), type)
							FOR XML PATH('Element'), type)
						FOR XML PATH('KnBasicAddressAdr'), type),
	
						(SELECT
							( SELECT
								( SELECT @Action as "@Action",
									(select top 1 land.Landcode
									 from dbo.land, dbo.relatie
									 where relatie.Postadres_Land = land.Land_ID)
									 AS CoId
									, 0 AS PbAd
									, (RTRIM((Postadres_1 + ' ' + ISNULL(Postadres_2,'')))) AS Ad
									, ISNULL(NULLIF(dbo.f_get_adrespart(Postadres_NR, 'nummer'),''),null) AS HmNr
									, ISNULL(NULLIF(dbo.f_get_adrespart(Postadres_NR, 'toevoeging'),''),null) AS HmAd
									, Postadres_Postcode AS ZpCd
									, Postadres_Plaats AS Rs
									, 0 AS ResZip
								FROM [dbo].[RELATIE]
								WHERE relatie.Relatie_NR = @RELATIE_NR AND
									  relatie.Relatietype = 16
								FOR XML PATH('Fields'), type)
							FOR XML PATH('Element'), type)
						FOR XML PATH('KnBasicAddressPad'), type)
					FOR XML PATH('Objects'), type
				) FOR XML PATH('Element'), type
			) FOR XML PATH ('KnOrganisation'), type
		) FOR XML PATH ('Objects'), type
	)FOR XML RAW ('Element'), ROOT ('KnPurchaseRelationOrg')
)

END

return @xsd

END



GO


