USE [RBS3                                                                                                                            ]
GO

/****** Object:  UserDefinedFunction [dbo].[f_get_adrespart]    Script Date: 4/24/2014 1:26:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[f_get_adrespart]( @adres varchar(100), @atype varchar(100)) returns varchar(100) 
begin 
       declare @apart varchar(100);

       if @atype = 'nummer' 
       begin
              set @apart = SUBSTRING(@adres,1, case when patindex('%[^0-9]%',@adres) = 0 then len(@adres) else patindex('%[^0-9]%',@adres) -1 end) ;
       end
       else
       begin
              set @apart = case when @adres like '%[a-Z]%' then 
              SUBSTRING(@adres,patindex('%[^0-9]%',@adres), len(@adres) ) 
              else null end;
       end;
       
       return @apart;
end;
GO

