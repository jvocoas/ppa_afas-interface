﻿USE [RBS3                                                                                                                            ]
GO

/****** Object:  Table [dbo].[AFAS_DATA]    Script Date: 17/02/2014 14:24:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFAS_DATA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RBS_RELATIE_NR] [varchar](50) NULL,
	[AFAS_ID_TRADE] [varchar](50) NULL,
	[AFAS_ID_SERVICE] [varchar](50) NULL,
	[AFAS_ID_UIENPOOL] [varchar](50) NULL,
 CONSTRAINT [PK_AFAS_DATA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO






USE [RBS3                                                                                                                            ]
GO

/****** Object:  Table [dbo].[AFAS_STATUS]    Script Date: 17/02/2014 14:25:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFAS_STATUS](
	[ID] [int] NOT NULL,
	[STATUS_DESCRIPTION] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AFAS_STATUS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO







USE [RBS3                                                                                                                            ]
GO

/****** Object:  Table [dbo].[AFAS_RUN]    Script Date: 17/02/2014 14:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AFAS_RUN](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JOB_START] [datetime] NOT NULL,
	[JOB_END] [datetime] NULL,
	[AFAS_STATUS] [int] NULL,
 CONSTRAINT [PK_AFAS_RUN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AFAS_RUN]  WITH CHECK ADD FOREIGN KEY([AFAS_STATUS])
REFERENCES [dbo].[AFAS_STATUS] ([ID])
GO









USE [RBS3                                                                                                                            ]
GO

/****** Object:  Table [dbo].[AFAS_MUTATIE]    Script Date: 17/02/2014 14:24:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFAS_MUTATIE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AFAS_RUN] [int] NULL,
	[MUTATIE_TYPE] [varchar](100) NOT NULL,
	[OMGEVING] [varchar](50) NOT NULL,
	[CREDITEUR_DEBITEUR] [varchar](50) NOT NULL,
	[RELATIE_ID] [varchar](50) NOT NULL,
	[FACTUUR_ID] [varchar](50) NULL,
	[XML] [varchar](4000) NULL,
	[EXCEPTION] [varchar](4000) NULL,
	[AFAS_STATUS] [int] NULL,
 CONSTRAINT [PK_AFAS_MUTATIE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AFAS_MUTATIE]  WITH CHECK ADD FOREIGN KEY([AFAS_RUN])
REFERENCES [dbo].[AFAS_RUN] ([ID])
GO

ALTER TABLE [dbo].[AFAS_MUTATIE]  WITH CHECK ADD FOREIGN KEY([AFAS_STATUS])
REFERENCES [dbo].[AFAS_STATUS] ([ID])
GO










