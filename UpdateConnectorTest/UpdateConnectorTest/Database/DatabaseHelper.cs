﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateConnectorTest.Database
{
    static class DatabaseHelper
    {
        /// <summary>
        /// Function to convert wildcards to Sql wildcards
        /// </summary>
        /// <param name="s"></param>
        /// <param name="escape"></param>
        /// <returns></returns>
        public static string ToSqlWildCard(string s,
                                              bool escape = false)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            else
            {
                s = s.Replace('*', '%');
                if (escape)
                    s = s.Replace("_", "\\_");
                return s.Replace('?', '_');
            }
        }


        public static bool IsNullOrEmpty(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return string.IsNullOrEmpty(Convert.ToString(reader[columnName]));
        }


        public static string GetString(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToString(reader[columnName]);
        }


        public static string GetString(this SqlParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");

            if (parameter.Value is SqlString)
            {
                SqlString s = (SqlString)parameter.Value;
                return s.IsNull ? null : s.Value;
            }
            else
                return null;
        }




        public static Int64 GetInt64(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToInt64(reader[columnName]);
        }



        public static double GetDouble(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToDouble(reader[columnName]);
        }


        public static Int64? GetNullableInt64(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            if (IsNullOrEmpty(reader, columnName))
                return null;
            else
                return Convert.ToInt64(reader[columnName]);
        }

        public static double? GetNullableDouble(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            if (IsNullOrEmpty(reader, columnName))
                return null;
            else
                return Convert.ToDouble(reader[columnName]);
        }

        public static Int64? GetInt64(this SqlParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            SqlDecimal d = (SqlDecimal)parameter.Value;
            return d.IsNull ? null : (Int64?)d.Value;
        }


        public static Int32 GetInt32(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToInt32(reader[columnName]);
        }


        public static Int32? GetInt32(this SqlParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            SqlDecimal d = (SqlDecimal)parameter.Value;
            return d.IsNull ? null : (Int32?)d.Value;
        }


        public static Int16 GetInt16(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToInt16(reader[columnName]);
        }


        public static Boolean GetBoolean(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToInt16(reader[columnName]) == 1;
        }


        public static Int16? GetInt16(this SqlParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            SqlDecimal d = (SqlDecimal)parameter.Value;
            return d.IsNull ? null : (Int16?)d.Value;
        }


        public static Boolean GetBoolean(this SqlParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            SqlDecimal d = (SqlDecimal)parameter.Value;
            return !d.IsNull && d.Value.Equals(1);
        }


        public static T GetEnum<T>(this SqlDataReader reader, string columnName) where T : struct, IConvertible
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return (T)Enum.Parse(typeof(T), reader[columnName].ToString(), true);
        }


        public static DateTime GetDateTime(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            return Convert.ToDateTime(reader[columnName]);
        }


        public static DateTime? GetNullableDateTime(this SqlDataReader reader, string columnName)
        {
            if (reader == null) throw new ArgumentNullException("reader");
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            if (IsNullOrEmpty(reader, columnName))
                return null;
            else
                return Convert.ToDateTime(reader[columnName]);
        }

        public static DateTime? GetDateTime(this SqlParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            SqlDateTime d = (SqlDateTime)parameter.Value;
            return d.IsNull ? null : (DateTime?)d.Value;
        }


        public static T GetEnum<T>(this SqlParameter parameter) where T : struct, IConvertible
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            return (T)Enum.Parse(typeof(T), parameter.Value.ToString(), true);
        }
    }
}
