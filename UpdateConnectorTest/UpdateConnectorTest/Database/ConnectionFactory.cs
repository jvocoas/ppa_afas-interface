﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace UpdateConnectorTest.Database
{
    /// <summary>
    /// Create SQL connections
    /// </summary>
    public class ConnectionFactory
    {
        /// <summary>
        /// Singleton, so hide the constructor 
        /// </summary>
        private ConnectionFactory() { }


        /// <summary>
        /// Factory instance
        /// </summary>
        public static ConnectionFactory Current
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        // Check to make sure it's null - for when two Threads are reaching this block in the same time
                        if (instance == null)
                        {
                            instance = new ConnectionFactory();
                        }
                    }
                }
                return instance;
            }
        }


        /// <summary>
        /// Returns the SQL Connection string
        /// </summary>
        public string SQLConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["sqlserverconstr"].ConnectionString;
            }
        }


        /// <summary>
        /// Create an Oracle connection
        /// </summary>
        /// <returns></returns>
        public SqlConnection CreateSqlConnection()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = this.SQLConnectionString;
            conn.Open();
            return conn;
        }

        private static object syncRoot = new Object();
        private static volatile ConnectionFactory instance;
    }
}
