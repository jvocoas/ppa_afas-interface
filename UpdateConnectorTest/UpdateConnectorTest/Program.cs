﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using UpdateConnectorTest.AFAS;
using System.Xml;
using System.Xml.Schema;
using UpdateConnectorTest;
using System.ServiceModel;
using System.IO;
using System.Xml.Serialization;
using UpdateConnectorTest.Classes;
using System.Xml.Linq;


namespace UpdateConnectorTest
{
    class Program
    {
        
        static void Main(string[] args)
        {

            UpdateConnector.CreateConnection();

            Console.WriteLine("Process has started.");

            AccessLog.NewAfasRun();
            List<string> relatienrupdate = LoadRelationToPush.GetRelatie_NR_Update();
            List<string> relatienrinsert = LoadRelationToPush.GetRelatie_NR_Insert();
            
            string xmlstring = string.Empty;
            string xsdstring = string.Empty;
            string creddeb = string.Empty;
            string omgeving = string.Empty;

            Console.WriteLine("Updating existing records.");

            int errorcount = 0;

            foreach (string s in relatienrupdate)
            {
                Console.WriteLine((relatienrupdate.IndexOf(s) + 1) + " of " + (relatienrupdate.Count + 1) + " : " + s);
                try // crediteur
                {
                    UpdateConnector.ExecuteWebservice("KNPurchaseRelationOrg", "crediteur", s, "Trade", "update");
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("General message: Inkooprelatie "))
                    {
                        try
                        {
                            UpdateConnector.ExecuteWebservice("KNPurchaseRelationOrg", "crediteur", s, "Trade", "insert");
                        }
                        catch (Exception ex2)
                        {
                            AccessLog.NewLogRecord(ex2.ToString(), UpdateConnector.xmlstring, UpdateConnector.type, s, UpdateConnector.creddeb, UpdateConnector.omgeving);
                            errorcount += 1;
                        }
                    }
                    else
                    {
                        AccessLog.NewLogRecord(ex.ToString(), UpdateConnector.xmlstring, UpdateConnector.type, s, UpdateConnector.creddeb, UpdateConnector.omgeving);
                        errorcount += 1;
                    }
                }

                try // debiteur
                {
                    UpdateConnector.ExecuteWebservice("KNSalesRelationOrg", "debiteur", s, "Trade", "update");
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("General message: Verkooprelatie "))
                    {
                        try
                        {
                            UpdateConnector.ExecuteWebservice("KNSalesRelationOrg", "debiteur", s, "Trade", "insert");
                        }
                        catch (Exception ex2)
                        {
                            AccessLog.NewLogRecord(ex2.ToString(), UpdateConnector.xmlstring, UpdateConnector.type, s, UpdateConnector.creddeb, UpdateConnector.omgeving);
                            errorcount += 1;
                        }
                    }
                    else
                    {
                        AccessLog.NewLogRecord(ex.ToString(), UpdateConnector.xmlstring, UpdateConnector.type, s, UpdateConnector.creddeb, UpdateConnector.omgeving);
                        errorcount += 1;
                    }
                }
            }
            
            Console.WriteLine("Updating " + relatienrupdate.Count + " relations, finished with " + errorcount + " errors. \n");
            
            Console.WriteLine("Inserting new records.");
      
            errorcount = 0;

            foreach (string s in relatienrinsert)
            {
                Console.WriteLine((relatienrinsert.IndexOf(s) + 1) + " of " + (relatienrinsert.Count + 1) + " : " + s);
                try // crediteur
                {
                    UpdateConnector.ExecuteWebservice("KNPurchaseRelationOrg", "crediteur", s, "Trade", "insert");
                }
                catch (Exception ex)
                {
                    AccessLog.NewLogRecord(ex.ToString(), UpdateConnector.xmlstring, UpdateConnector.type, s, UpdateConnector.creddeb, UpdateConnector.omgeving);
                    errorcount += 1;
                }

                try // debiteur
                {
                    UpdateConnector.ExecuteWebservice("KNSalesRelationOrg", "debiteur", s, "Trade", "insert");
                }
                catch (Exception ex)
                {
                    AccessLog.NewLogRecord(ex.ToString(), UpdateConnector.xmlstring, UpdateConnector.type, s, UpdateConnector.creddeb, UpdateConnector.omgeving);
                    errorcount += 1;
                }
            }



            AccessLog.UpdateAfasRun(errorcount);

            Console.WriteLine("Inserting " + relatienrinsert.Count + " records, finished with " + errorcount + " errors.\n");
            Console.WriteLine("Process has finished");
            Console.ReadKey();
            }


        }
    }