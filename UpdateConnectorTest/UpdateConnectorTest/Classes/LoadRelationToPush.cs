﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using UpdateConnectorTest.Database;


namespace UpdateConnectorTest.Classes
{
    class LoadRelationToPush
    {
        public static string GetXMLForRelation(string Relatie_NR, string action, string creddeb, string omgeving)
        {
            
            string result = "";

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    if(creddeb == "crediteur")
                    {
                        cmd.CommandText = @"SELECT dbo.f_get_xml_afas_update_crediteur(@Relatie_NR, @action, @omgeving)";
                    }
                    else if (creddeb == "debiteur")
                    {
                        cmd.CommandText = @"SELECT dbo.f_get_xml_afas_update_debiteur(@Relatie_NR, @action, @omgeving)";
                    }
                    
                    cmd.Parameters.AddWithValue("Relatie_NR", Relatie_NR);
                    cmd.Parameters.AddWithValue("action", action);
                    cmd.Parameters.AddWithValue("omgeving", omgeving);
                
                    using(SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr.IsDBNull(0))
                            {
                                return null;
                            }
                            else
                            {
                                result = dr.GetString(0);
                            }
                                
                        }
                    }
                }
            }
            return result;
        }

        public static string GetXSDForRelation(string Relatie_NR, string action, string creddeb, string omgeving)
        {

            string result = "";

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    if (creddeb == "crediteur")
                    {
                        cmd.CommandText = @"SELECT dbo.f_get_xsd_afas_update_crediteur(@Relatie_NR, @action, @omgeving)";
                    }
                    else if (creddeb == "debiteur")
                    {
                        cmd.CommandText = @"SELECT dbo.f_get_xsd_afas_update_debiteur(@Relatie_NR, @action, @omgeving)";
                    }

                    cmd.Parameters.AddWithValue("Relatie_NR", Relatie_NR);
                    cmd.Parameters.AddWithValue("action", action);
                    cmd.Parameters.AddWithValue("omgeving", omgeving);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr.IsDBNull(0))
                            {
                                return null;
                            }
                            else
                            {
                                result = dr.GetString(0);
                            }
                        }
                    }
                }
            }
            return result;
        }




        public static List<string> GetRelatie_NR_Update ()
        {

            List<string> result = new List<string>();

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT relatie.Relatie_NR
                                        FROM relatie
                                        WHERE ( Datum_Mutatie > pushed_to_afas AND
                                                Relatietype = 16)";
                    
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.Add(dr.GetString(0));
                        }
                    }
                }
            }
            return result;
        }


        public static List<string> GetRelatie_NR_Insert()
        {

            List<string> result = new List<string>();

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT relatie.Relatie_NR
                                        FROM relatie
                                        WHERE ( pushed_to_afas IS NULL AND
                                                Relatietype = 16)";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.Add(dr.GetString(0));
                        }
                    }
                }
            }
            return result;
        }
    
    
    }
}
