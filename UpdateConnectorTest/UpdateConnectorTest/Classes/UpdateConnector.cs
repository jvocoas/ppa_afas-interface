﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UpdateConnectorTest.AFAS;

namespace UpdateConnectorTest.Classes
{
    class UpdateConnector
    {

        static UpdateConnectorSoapClient client = new UpdateConnectorSoapClient();
        public static string xmlstring = string.Empty;
        public static string omgeving = string.Empty;
        public static string creddeb = string.Empty;
        public static string type = string.Empty;


        public static void CreateConnection()
        {
            client.ClientCredentials.Windows.AllowNtlm = true;
            client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("48211.admin", "PP311rkt", "");
        }


        public static void ExecuteWebservice(string connectorid, string creddeb, string relatienr, string omgeving, string type)
        {
            
            string xsdstring = string.Empty;
            UpdateConnector.type = type;
            UpdateConnector.omgeving = omgeving;
            UpdateConnector.creddeb = creddeb;
            UpdateConnector.xmlstring = LoadRelationToPush.GetXMLForRelation(relatienr, type, creddeb, omgeving);

            if (xmlstring != null)
            {
                xsdstring = LoadRelationToPush.GetXSDForRelation(relatienr, type, creddeb, omgeving);

                client.Execute("O48211AF", "48211.admin", "PP311rkt", "", connectorid, 1, xsdstring);    //TRADE TEST 

                AccessLog.UpdatePushedToAfas(relatienr);
                AccessLog.NewLogRecord("", xmlstring, type, relatienr, creddeb, omgeving);
            }

        }

    }
}



//             client.Execute("O48211AF", "48211.admin", "PP311rkt", "", "KNSalesRelationOrg", 1, xsdstring);    //TRADE TEST    
//             client.Execute("O48211AG", "48211.admin", "PP311rkt", "", "KNOrganisation", 1, xsdstring);    //SERVICE TEST
//             client.Execute("O48211AH", "48211.admin", "PP311rkt", "", "KNOrganisation", 1, xsdstring);    //UIENPOOL TEST