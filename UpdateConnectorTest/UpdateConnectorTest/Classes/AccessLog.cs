﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UpdateConnectorTest.Database;

namespace UpdateConnectorTest.Classes
{
    class AccessLog
    {


        private static int afasrun_id = 0;

        public static void NewLogRecord(string ex, string xmldata, string type, string relatieid, string creddeb, string omgeving)
        {
            string afasid = GetAfasID(relatieid);

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"INSERT INTO [dbo].[AFAS_MUTATIE](
                                                        [AFAS_RUN],
                                                        [MUTATIE_TYPE],
                                                        [OMGEVING],
                                                        [CREDITEUR_DEBITEUR],
                                                        [RELATIE_ID],
                                                        [XML],
                                                        [EXCEPTION],
                                                        [AFAS_STATUS])
                                        VALUES (@afasrun, @type, @omgeving, @creddeb, @relatieid, @xml, @exception, @afasstatus)";

                    cmd.Parameters.AddWithValue("afasrun", afasrun_id);
                    cmd.Parameters.AddWithValue("type", type);
                    cmd.Parameters.AddWithValue("omgeving", omgeving);
                    cmd.Parameters.AddWithValue("creddeb", creddeb);
                    cmd.Parameters.AddWithValue("relatieid", relatieid);
                    if(string.IsNullOrEmpty(xmldata))
                    {
                        cmd.Parameters.AddWithValue("xml", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("xml", xmldata);
                    }
                    
                    if (string.IsNullOrEmpty(ex))
                    {
                        cmd.Parameters.AddWithValue("exception", "");
                        cmd.Parameters.AddWithValue("afasstatus", 10);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("exception", ex);
                        cmd.Parameters.AddWithValue("afasstatus", 20);
                    }

                    cmd.ExecuteNonQuery();

                }
            }
        }


        public static string GetAfasID(string relatieID)
        {
            string afasID = string.Empty;

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT [AUTONUMMER] + 10000 as AFASID
                                        FROM [dbo].[RELATIE]
                                        WHERE [RELATIE_NR] = @relatieid AND
                                              [RELATIETYPE] = 16";
                    cmd.Parameters.AddWithValue("relatieid", relatieID);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            afasID = dr.GetString("AFASID");
                        }
                    }
                }
            }
            return afasID;
        }


        public static void NewAfasRun()
        {

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"INSERT INTO [dbo].[AFAS_RUN]([JOB_START]) VALUES (SYSDATETIME())";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = @"SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            afasrun_id = dr.GetInt16("SCOPE_IDENTITY");
                        }
                    }
                }
            }
        }


        public static void UpdateAfasRun(int errorcount)
        {

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"UPDATE [dbo].[AFAS_RUN]
                                        SET [JOB_END] = SYSDATETIME(),
                                            [AFAS_STATUS] = @afas_status_id
                                        WHERE ID = @afasrun_id";
                    cmd.Parameters.AddWithValue("afasrun_id", afasrun_id);
                    if (errorcount == 0)
                        cmd.Parameters.AddWithValue("afas_status_id", 10);
                    else
                        cmd.Parameters.AddWithValue("afas_status_id", 20);

                    cmd.ExecuteNonQuery();
                }
            }
        }



        public static void UpdatePushedToAfas(string relatieID)
        {

            using (SqlConnection conn = ConnectionFactory.Current.CreateSqlConnection())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"UPDATE [dbo].[RELATIE]
                                        SET [PUSHED_TO_AFAS] = SYSDATETIME()
                                        WHERE [RELATIE_NR] = @relatieid AND
                                              [RELATIETYPE] = 16";
                    cmd.Parameters.AddWithValue("relatieid", relatieID);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        
    }
}
